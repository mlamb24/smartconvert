package us.sheeplabs.smartconvert;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;

public class MainMenu extends Activity {
	
	private EditText editText;
	public final static String input = "us.sheeplabs.SmartConvert.MESSAGE";
	private Typeface typeFace;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);
		editText = (EditText) findViewById(R.id.edit_message);
		
		typeFace = Typeface.createFromAsset(getAssets(),"fonts/RobGraves.ttf");
		editText.setTypeface(typeFace);
		
		editText.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode==KeyEvent.KEYCODE_ENTER) {
                	smartConButton(v);
                	return true;
                }
                return false;
            }
		});
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }
    
	public void smartConButton(View view) {
		Intent intent = new Intent(this, ResultsScreen.class);
		String message;
		if (editText.getText()!=null)
			message = editText.getText().toString();
		else
			message = "No Input Given";
		intent.putExtra(input, message);
    	startActivity(intent);
	}
	
	public void aboutButton(View view){
		Intent intent = new Intent(this, About.class);
    	startActivity(intent);
	}
	
	public void helpButton(View view){
		Intent intent = new Intent(this, Help.class);
    	startActivity(intent);
	}
}
