package us.sheeplabs.smartconvert.Test;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import us.sheeplabs.smartconvert.Converters.Units.Unit;
import us.sheeplabs.smartconvert.Converters.Units.UnitLibrary;
import us.sheeplabs.smartconvert.Converters.Units.UnitType;

/**
 * Generates an HTML file which documents valid Units and UnitTypes.
 * 
 * @author Michael Lambert
 * 
 */
public class HelpDocGenerator {
	/**
	 * Generates an HTML file.
	 */
	private static void createDocumentation() {
		try {
			// File Creation
			File helpDoc = new File("HelpDoc.html");

			FileWriter fw = new FileWriter(helpDoc);
			BufferedWriter bw = new BufferedWriter(fw);
			StringBuilder names;

			// Basic HTML skeleton
			bw.write("<!DOCTYPE html>\n<html>\n<head>"
					+ "\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />"
					+ "\n\t<title>Help Documentation</title>"
					+ "\n</head>\n<body>");
			bw.write("\n<h1><a name=\"#top\">Help Documentation:</a></h1>");

			// Links to Unit Types
			bw.write("\n<ul>");
			for (UnitType type : UnitType.values()) {
				bw.write("\n\t<li><a href=\"#" + type.getName() + "\">"
						+ type.getName() + "</a>");
			}
			bw.write("\n</ul>");

			// List of Unit Types
			bw.write("\n<ul>");
			for (UnitType type : UnitType.values()) {
				// List of Units
				bw.write("\n<li><a name=\"" + type.getName() + "\">"
						+ type.getName() + "</a>\n\t<ul>");
				for (Unit unit : UnitLibrary.getUnitSet(type)) {
					bw.write("\n\t\t<li>" + unit.getLabel());
					// Unit Specifications
					bw.write("\n\t\t\t<ul>");
					bw.write("\n\t\t\t\t<li><b>Conversion Factor:</b> "
							+ unit.getFactor() + "</li>");
					bw.write("\n\t\t\t\t<li><b>Alternate Names:</b>");
					// Unit Names
					names = new StringBuilder();
					for (String name : unit.getNames()) {
						names.append(" " + name + ",");
					}
					names.deleteCharAt(names.length() - 1); // Easier than
															// counting units,
															// since HashSet is
															// used.
					bw.write(names.toString());
					bw.write("\n\t\t\t</ul>");
					bw.write("\n\t\t</li>");
				}
				bw.write("\n\t</ul>\n</li>");
				bw.write("\n<br>\n<a href=\"#top\">Back to Top</a>\n<br><br>");
			}

			// Closing HTML document.
			bw.write("\n</ul>\n</body>\n</html>");

			bw.flush();
			bw.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		createDocumentation();
	}
}
