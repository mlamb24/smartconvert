package us.sheeplabs.smartconvert.Test;

import java.util.Arrays;

import us.sheeplabs.smartconvert.Converters.Converter;
import us.sheeplabs.smartconvert.Converters.Units.Unit;
import us.sheeplabs.smartconvert.Converters.Units.UnitLibrary;
import us.sheeplabs.smartconvert.Converters.Units.UnitType;
import us.sheeplabs.smartconvert.Parser.Parser;

public class Test {

	/**
	 * Tests to see if unit enums are properly retrievable from Strings.
	 */
	public static void unitTest() {
		// Acceleration
		Unit unitTest = UnitLibrary.getFromName("mpers2");
		assert (unitTest.getLabel().equals("Meters/sq.sec (m/s\u00b2)"));

		unitTest = UnitLibrary.getFromName("meterpersqsecond");
		assert (unitTest.getLabel().equals("Meters/sq.sec (m/s\u00b2)"));

		unitTest = UnitLibrary.getFromName("feetpersecondsq");
		assert (unitTest.getLabel().equals("Feet/sq.sec (ft/sec\u00b2)"));

		unitTest = UnitLibrary.getFromName("gravities");
		assert (unitTest.getLabel().equals("Gravitational Force (g)"));

		unitTest = UnitLibrary.getFromName("Gals");
		assert (unitTest.getLabel().equals("Galileos (cm/sec\u00b2)"));

		unitTest = UnitLibrary.getFromName("ins-2");
		assert (unitTest.getLabel().equals("Inches/sq.sec (in/sec\u00b2)"));

		unitTest = UnitLibrary.getFromName("yardspersecondsq");
		assert (unitTest.getLabel().equals("Yards/sq.sec (yd/sec\u00b2)"));

		System.out.println("Acceleration Passed");

		// Length
		unitTest = UnitLibrary.getFromName("angstrom");
		assert (unitTest.getLabel().equals("\u00C5ngstr\u00F6ms (\u00C5)"));

		unitTest = UnitLibrary.getFromName("metre");
		assert (unitTest.getLabel().equals("Meters/Metres (m)"));

		unitTest = UnitLibrary.getFromName("cables");
		assert (unitTest.getLabel().equals("Cable Lengths (US Navy)"));

		try {
			unitTest = UnitLibrary.getFromName("invalidunit");
		} catch (ClassCastException i) {
			System.out.println("Length Passed");
		}

		// Electric Charge
		unitTest = UnitLibrary.getFromName("C");
		assert (unitTest.getLabel().equals("Coloumbs (C)"));

		unitTest = UnitLibrary.getFromName("abC");
		assert (unitTest.getLabel().equals("Abcoulombs (abC)"));

		unitTest = UnitLibrary.getFromName("elementarycharges");
		assert (unitTest.getLabel().equals("Elementary Charges"));

		System.out.println("Charge Passed");

		// Time
		unitTest = UnitLibrary.getFromName("s");
		assert (unitTest.getLabel().equals("Seconds (s)"));

		unitTest = UnitLibrary.getFromName("galyear");
		assert (unitTest.getLabel().equals("Galactic Years"));

		unitTest = UnitLibrary.getFromName("eon");
		assert (unitTest.getLabel().equals("Eons"));

		System.out.println("Time Passed");
	}
	
	public static void libraryLengths() {
		for (UnitType ut: UnitType.values()) {
			System.out.println(UnitLibrary.getUnitSet(ut).size());
		}
	}

	/**
	 * Tests to see if units are returning the proper conversion table when
	 * given the base unit of the enum. Then tests if 1 fortnight is properly
	 * converted to days and minutes, with a repeat request being ignored.
	 */
	public static void conversionTest() {
		Converter accel = new Converter(1, UnitLibrary.getFromName("mpersqs"));
		System.out.println(Arrays.toString(accel.solve()));

		Converter charge = new Converter(1, UnitLibrary.getFromName("C"));
		System.out.println(Arrays.toString(charge.solve()));

		Converter current = new Converter(1, UnitLibrary.getFromName("A"));
		System.out.println(Arrays.toString(current.solve()));

		Converter length = new Converter(1, UnitLibrary.getFromName("m"));
		System.out.println(Arrays.toString(length.solve()));

		Converter mass = new Converter(1, UnitLibrary.getFromName("g"));
		System.out.println(Arrays.toString(mass.solve()));

		Converter memory = new Converter(1, UnitLibrary.getFromName("bit"));
		System.out.println(Arrays.toString(memory.solve()));

		Converter time = new Converter(1, UnitLibrary.getFromName("s"));
		System.out.println(Arrays.toString(time.solve()));

		String[] testArray = { "min", "minutes", "days" };
		time = new Converter(1, UnitLibrary.getFromName("fortnight"), testArray);
		System.out.println(Arrays.toString(time.solve()));
	}

	/**
	 * Tests if the parser correctly can identify a vague and specific
	 * conversion.
	 */
	public static void parserTest() {
		Converter a = Parser.parseInput("12 mm to meters");
		System.out.println(Arrays.toString(a.solve()));

		a = Parser.parseInput("12 mm");
		System.out.println(Arrays.toString(a.solve()));
	}

	/**
	 * Runs the tests.
	 */
	public static void main(String[] args) {
		unitTest();
		libraryLengths();
		conversionTest();
		parserTest();
	}
}
