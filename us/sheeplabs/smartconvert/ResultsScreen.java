package us.sheeplabs.smartconvert;

import us.sheeplabs.smartconvert.Converters.Converter;
import us.sheeplabs.smartconvert.Parser.InvalidInputException;
import us.sheeplabs.smartconvert.Parser.Parser;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup.LayoutParams;
import android.widget.LinearLayout;
import android.widget.TextView;

public class ResultsScreen extends Activity {

	private Converter converter;
	private String input;

	private LinearLayout layout;
	private LinearLayout.LayoutParams labelParam;
	private LinearLayout.LayoutParams boxParam;
	private Typeface typeFace;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_results_screen);
		typeFace = Typeface.createFromAsset(getAssets(),"fonts/RobGraves.ttf");
		Intent intent = getIntent();
		if (intent.getStringExtra(MainMenu.input) != null)
			this.input = intent.getStringExtra(MainMenu.input);
		else
			this.input = "No Input Found";
		displaySolution();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.results_screen, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * Displays the solution and catches various input/solving errors.
	 */
	private void displaySolution() {
		layout = (LinearLayout) this.findViewById(R.id.layout_main);
		DisplayMetrics metrics = layout.getContext().getResources().getDisplayMetrics();
		int dp25 = (int) (metrics.density * 25f + 0.5f);
		int dp12 = (int) (metrics.density * 12f + 0.5f);
		int dp10 = (int) (metrics.density * 10f + 0.5f);
		int dp4 = (int) (metrics.density * 4f + 0.5f);
		
		labelParam = new LinearLayout.LayoutParams(layout.getLayoutParams());
		labelParam.setMargins(dp10, dp12, dp10, 0);

		boxParam = new LinearLayout.LayoutParams(layout.getLayoutParams());
		boxParam.setMargins(dp25, dp4, dp10, 0);

		createResult("Input:", input);
		
		//Easter Eggs
		if (input.equals("How are you doing?")) {
			createResult("How I'm Doing:", "I'm very well. Thank you for asking!");
			return;
		}
		if (input.equals("Easter Egg")) {
			createResult("Easter Egg:", "lolEasterEgg");
			return;
		}
		
		if (input.equals("a problem")) {
			createResult("Well,", "Aren't we a clever one?");
		}
		
		//Real results
		try {
			converter = Parser.parseInput(input);
			String[] results = converter.solve();
			String[] labels = converter.getLabels();

			createResult("Interpretation:", converter.getInterpretation());
			
			for (int r = 0; r < results.length; r++) {
				if (!(results[r] == null || results[r].equals("")))
					createResult(labels[r] + ":", results[r]);
				else
					break;
			}
			if (converter.getErrorCount() > 0) {
				String[] errors = converter.getErrors();
				for (int r = 0; r < converter.getErrorCount(); r++) {
					if (!(errors[r] == null || errors[r].equals("")))
						createResult("Error:", errors[r]);
					else
						break;
				}
			}
		}
		// Invalid Inputs & Special Cases
		catch (InvalidInputException iie) {
			createResult("Error:", iie.getMessage());
		}
	}

	private void createResult(String label, String box) {
		// Label
		final TextView nextLabel = new TextView(this);
		// Text
		nextLabel.setText(label);
		nextLabel.setTypeface(typeFace);
		nextLabel.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
		nextLabel.setTextColor(getResources().getColor(R.color.white));

		// Layout
		nextLabel.setLayoutParams(new LayoutParams(-2, -2));

		// Box
		final TextView nextBox = new TextView(this);

		// Text
		nextBox.setText(box);
		nextBox.setTypeface(typeFace);
		nextBox.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
		nextBox.setTextColor(getResources().getColor(R.color.white));
		nextBox.setTextIsSelectable(true);
		// Layout
		nextBox.setLayoutParams(new LayoutParams(-2, -2));

		// Adds the created Views.
		layout.addView(nextLabel, labelParam);
		layout.addView(nextBox, boxParam);
	}
}
