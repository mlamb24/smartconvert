package us.sheeplabs.smartconvert.Converters.Units;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Holds a library of all units, stored in ArrayLists as to make iteration
 * efficient through the various unit types. <br>
 * <br>
 * Rules for validNames:
 * <ul>
 * <li>The parser removes all whitespace from the given input.</li>
 * <li>Makes all input of 6 or more characters lowercase.</li>
 * <li>"metre" is automatically converted in parser to "metre"</li>
 * <li>"squared" and "square" become "sq"</li>
 * <li>"cubed", "cube" and "cubic" become "cu"</li>
 * </ul>
 * 
 * @author Michael Lambert
 * 
 */
public class UnitLibrary {
	// ACCELERATION UNITS ------------------------------------------------------
	private final static Unit MSECSQ = new Unit(1.0, UnitType.ACCELERATION,
			"Meters/sq.sec (m/s\u00b2)", "mpers2", "ms-2", "mpersec2",
			"mpersqsec", "mpersecond2", "mpersqs", "meterspersecondsq",
			"meterpersecondsq", "meterspersqsecond", "meterpersqsecond",
			"meterpersecond2", "meterspersecond2");

	private final static Unit MMINSQ = new Unit(3600.0, UnitType.ACCELERATION,
			"Meters/sq.min (m/min\u00b2)", "mpermin2", "mmin-2", "mpermin2",
			"mpersqmin", "mperminute2", "metersperminutesq",
			"meterperminutesq", "meterspersqminute", "meterpersqminute",
			"meterperminute2", "metersperminute2");

	private final static Unit MHRSQ = new Unit(7.716e-8, UnitType.ACCELERATION,
			"Meters/sq.hr (m/hr\u00b2)", "mperhr2", "mhr-2", "mperhr2",
			"mpersqhr", "mperhour2", "metersperhoursq", "meterperhoursq",
			"meterspersqhour", "meterpersqhour", "meterperhour2",
			"metersperhour2");

	private final static Unit KMHRSQ = new Unit(7.716e-5,
			UnitType.ACCELERATION, "Kilometers/sq.hr (m/hr\u00b2)", "kmperhr2",
			"kmhr-2", "kmperhr2", "kmpersqhr", "kmperhour2",
			"kilometersperhoursq", "kilometerperhoursq", "kilometerspersqhour",
			"kilometerpersqhour", "kilometerperhour2", "kilometersperhour2");

	private final static Unit FTSECSQ = new Unit(0.3048, UnitType.ACCELERATION,
			"Feet/sq.sec (ft/sec\u00b2)", "ftpers2", "ftpersqs", "ftpers2",
			"ftsec-2", "fts-2", "ftpersec2", "ftpersqsec", "ftpersecond2",
			"footpersecondsq", "feetpersecondsq", "footpersqsecond",
			"feetpersqsecond", "footpersecond2", "feetpersecond2");

	private final static Unit FTMINSQ = new Unit(8.4666666e-5,
			UnitType.ACCELERATION, "Feet/sq.min (ft/min\u00b2)", "ftpermin2",
			"ftmin-2", "ftpermin2", "ftpersqmin", "ftperminute2",
			"footperminutesq", "feetperminutesq", "feetpersqminute",
			"footpersqminute", "footperminute2", "feetperminute2");

	private final static Unit FTHRSQ = new Unit(2.3518518e-8,
			UnitType.ACCELERATION, "Feet/sq.hr (ft/hr\u00b2)", "ftperhr2",
			"fthr-2", "ftperhr2", "ftpersqhr", "ftperhour2", "footperhoursq",
			"feetperhoursq", "feetpersqhour", "footpersqhour", "footperhour2",
			"feetperhour2");

	private final static Unit GRAV = new Unit(9.80665, UnitType.ACCELERATION,
			"Gravitational Force (g)", "g", "gs", "gravity", "gravities",
			"grav", "gravs");

	// Galileo is the official name for the unit cm/sec^2
	private final static Unit GAL = new Unit(0.01, UnitType.ACCELERATION,
			"Galileos (cm/sec\u00b2)", "Gal", "Gals", "galileo", "galileos",
			"cmpers2", "cms-2", "cmpersec2", "cmpersqsec", "cmpersecond2",
			"cmpersqs", "centimeterspersecondsq", "centimeterpersecondsq",
			"centimeterspersqsecond", "centimeterpersqsecond",
			"centimeterpersecond2", "centimeterspersecond2");

	private final static Unit INSECSQ = new Unit(0.0254, UnitType.ACCELERATION,
			"Inches/sq.sec (in/sec\u00b2)", "inpers2", "inpersqs", "ins-2",
			"inpersec2", "inpersqsec", "inpersecond2", "insec-2",
			"inchpersecondsq", "inchespersecondsq", "inchpersqsecond",
			"inchespersqsecond", "inchpersecond2", "inchespersecond2");

	private final static Unit INMINSQ = new Unit(7.056e-6,
			UnitType.ACCELERATION, "Inches/sq.min (in/min\u00b2)", "inpers2",
			"inpersqs", "ins-2", "inpermin2", "inpersqmin", "inperminute2",
			"inmin-2", "inchperminutesq", "inchesperminutesq",
			"inchpersqminute", "inchespersqminute", "inchperminute2",
			"inchesperminute2");

	private final static Unit YDSECSQ = new Unit(0.9144, UnitType.ACCELERATION,
			"Yards/sq.sec (yd/sec\u00b2)", "ydpers2", "ydpersqs", "ypers2",
			"yds-2", "ydpersec2", "ydpersqsec", "ydpersecond2",
			"yardpersecondsq", "yardspersecondsq", "yardpersqsecond",
			"yardspersqsecond", "yardpersecond2", "yardspersecond2");

	private final static Unit MISECSQ = new Unit(1609.34348501,
			UnitType.ACCELERATION, "Miles/sq.sec (mi/sec\u00b2)", "mipers2",
			"mipersqs", "mis-2", "mipersec2", "mipersqsec", "ymipersecond2",
			"milepersecondsq", "milespersecondsq", "milepersqsecond",
			"milespersqsecond", "milepersecond2", "milespersecond2");

	protected final static ArrayList<Unit> ACCELERATION_UNITS = new ArrayList<Unit>(
			Arrays.asList(MSECSQ, MMINSQ, MHRSQ, KMHRSQ, FTSECSQ, FTMINSQ,
					FTHRSQ, GRAV, GAL, INSECSQ, INMINSQ, YDSECSQ, MISECSQ));

	// CHARGE UNITS ------------------------------------------------------------
	// Common SI Units
	private final static Unit C = new Unit(1, UnitType.CHARGE, "Coloumbs (C)",
			"C", "as", "asec", "asecs", "a*s", "coloumb", "coloumbs",
			"ampere-second", "ampere-seconds", "amperesecond", "ampereseconds",
			"amp-second", "amp-seconds", "ampsecond", "ampseconds");

	private final static Unit MC = new Unit(1e-3, UnitType.CHARGE,
			"Millicoloumbs (mC)", "mC", "millicoloumb", "millicoloumbs");

	private final static Unit MICROC = new Unit(1e-6, UnitType.CHARGE,
			"Microcoloumbs (\u00B5C)", "\u00B5C", "microcoloumb",
			"microocoloumbs");

	private final static Unit NC = new Unit(1e-9, UnitType.CHARGE,
			"Nanocoloumbs (nC)", "nC", "nanocoloumb", "nanocoloumbs");

	private final static Unit PC = new Unit(1e-12, UnitType.CHARGE,
			"Picocoloumbs (pC)", "pC", "picocoloumb", "pciocoloumbs");

	// Others
	private final static Unit ABC = new Unit(10, UnitType.CHARGE,
			"Abcoulombs (abC)", "abC", "abcoloumb", "abcoloumbs");

	private final static Unit STATC = new Unit(3.336e-10, UnitType.CHARGE,
			"Statcoloumbs/Franklins (statC/Fr)", "statC", "Fr", "statcoloumb",
			"statcoloumbs");

	private final static Unit AH = new Unit(36e2, UnitType.CHARGE,
			"Amepere-hours (Ah)", "Ah", "Ahr", "Ahrs", "ampere-hour",
			"ampere-hours", "amp-hour", "amp-hours", "amperehour",
			"amperehours", "amphour", "amphours");

	private final static Unit F = new Unit(96485.3365, UnitType.CHARGE,
			"Faradays (F)", "F", "faraday", "faradays", "historicalfaraday",
			"historicalfaradays");

	private final static Unit PCHARGE = new Unit(1.8755459e-18,
			UnitType.CHARGE, "Planck Charges", "planckcharge", "plankcharges");

	private final static Unit ELEMENTARY = new Unit(1.602176565e-19,
			UnitType.CHARGE, "Elementary Charges", "elementarycharge",
			"elementarycharges", "elemcharge", "elemcharges");

	private final static ArrayList<Unit> CHARGE_UNITS = new ArrayList<Unit>(
			Arrays.asList(C, MC, MICROC, NC, PC, ABC, STATC, AH, F, PCHARGE,
					ELEMENTARY));

	// CURRENT UNITS -----------------------------------------------------------
	// Common SI Units
	private static final Unit A = new Unit(1, UnitType.CURRENT, "Amperes (A)",
			"A", "cpers", "cpersec", "ampere", "amperes", "coloumbpersecond",
			"coloumbspersecond", "voltperohm", "voltsperohm", "wattpervolt",
			"wattspervolt", "WeberperHenry", "WebersperHenry");

	private static final Unit KA = new Unit(1e3, UnitType.CURRENT,
			"Kiloamperes (kA)", "kA", "kiloampere", "kiloamperes");

	private static final Unit MGA = new Unit(1e6, UnitType.CURRENT,
			"Megaamperes (MA)", "MA", "megaampere", "megaamperes");

	private static final Unit GA = new Unit(1e9, UnitType.CURRENT,
			"Gigaamperes (TA)", "GA", "gigaampere", "gigaamperes");

	private static final Unit TA = new Unit(1e12, UnitType.CURRENT,
			"Teraamperes (TA)", "TA", "teraampere", "teraamperes");

	private static final Unit MA = new Unit(1e-3, UnitType.CURRENT,
			"Milliamperes (mA)", "mA", "milliampere", "milliamperes");

	private static final Unit MICROA = new Unit(1e-6, UnitType.CURRENT,
			"Microamperes (\u00B5A)", "\u00B5A", "microampere", "microoamperes");

	private static final Unit NA = new Unit(1e-9, UnitType.CURRENT,
			"Nanoamperes (nA)", "nA", "nanoampere", "nanoamperes");

	private static final Unit PA = new Unit(1e-12, UnitType.CURRENT,
			"Picoamperes (pA)", "pA", "picoampere", "pcioamperes");

	// Others
	private static final Unit BIOT = new Unit(10, UnitType.CURRENT,
			"Biots/Abamperes/Electromagnetic Units of Current (aA)", "aA",
			"biot", "biots", "abampere", "abamperes",
			"electromagneticunitofcurrent", "electromagneticunitsofcurrent",
			"electromagneticunit", "electromagneticunits");

	private static final Unit ELECTROSTATIC = new Unit(3.35641E-10,
			UnitType.CURRENT, "Franklins/Second"
					+ " (Electrostatic Units of Current, Gauss, statampere)",
			"G", "gauss", "gausses", "frpers", "franklinpersecond",
			"franklinspersecond", "electrostaticunitofcurrent",
			"electrostaticunitsofcurrent", "electrostaticunit",
			"electrostaticunits");

	private static final Unit GI = new Unit(0.795774715, UnitType.CURRENT,
			"Gilbert (Gi)", "Gi", "gilbert", "gilberts");

	private final static ArrayList<Unit> CURRENT_UNITS = new ArrayList<Unit>(
			Arrays.asList(A, KA, MGA, GA, TA, MA, MICROA, NA, PA, BIOT,
					ELECTROSTATIC, GI));

	// LENGTH UNITS ------------------------------------------------------------
	// SI Units
	private static final Unit M = new Unit(1, UnitType.LENGTH,
			"Meters/Metres (m)", "m", "meter", "metre", "meters", "metres");

	private static final Unit KM = new Unit(1e4, UnitType.LENGTH,
			"Kilometers/Kilometres (km)", "km", "kilometer", "kilometers",
			"kilometre", "kilometres");

	private static final Unit CM = new Unit(1e-2, UnitType.LENGTH,
			"Centimeters/Centimetres (cm)", "cm", "centimeter", "centimeters",
			"centimetre", "centimetres");

	private static final Unit MM = new Unit(1e-3, UnitType.LENGTH,
			"Millimeters/Millimetres (mm)", "mm", "millimeter", "millimeters",
			"millimetre", "millimetres");

	private static final Unit MICROM = new Unit(1e-6, UnitType.LENGTH,
			"Microns/Micrometers/Micrometres (\u00B5m)", "\u00B5m", "micron",
			"micron", "micrometer", "microometers", "micrometre", "micrometres");

	private static final Unit NM = new Unit(1e-9, UnitType.LENGTH,
			"Nanometers/Nanometres (nm)", "nm", "nanometer", "nanometers",
			"nanometre", "nanometres");

	private static final Unit FM = new Unit(1e-15, UnitType.LENGTH,
			"Femtometers/Femtometres/Fermis (fm)", "fm", "fermi", "fermis",
			"femtometer", "femtometers", "femtometre", "femtometres");

	// Non-SI
	private static final Unit ARING = new Unit(1e-10, UnitType.LENGTH,
			"\u00C5ngstr\u00F6ms (\u00C5)", "\u00C5", "\u00E5", "angstrom",
			"angstroms", "\u00E5ngstr\u00F6m", "\u00E5ngstr\u00F6ms",
			"angstr\u00F6m", "angstr\u00F6ms", "\u00E5ngstrom",
			"\u00E5ngstroms");

	// US
	private static final Unit IN = new Unit(.0254, UnitType.LENGTH,
			"Inches (in)", "in", "inch", "inches");

	private static final Unit THOU = new Unit(2.54e-5, UnitType.LENGTH,
			"Thous/Mils", "mil", "mils", "thou", "thous");

	private static final Unit FT = new Unit(0.3048, UnitType.LENGTH,
			"Feet (ft)", "ft", "foot", "feet");

	private static final Unit YD = new Unit(0.9144, UnitType.LENGTH,
			"Yards (yd)", "yd", "yard", "yards");

	private static final Unit MI = new Unit(1609.34, UnitType.LENGTH,
			"Terrestrial Miles (mi)", "mi", "termi", "mile", "miles");

	private static final Unit LEAGUE = new Unit(5556, UnitType.LENGTH,
			"Leagues", "league", "leagues");

	// English
	private static final Unit LINE = new Unit(2.11667e-3, UnitType.LENGTH,
			"Lines/Poppyseeds", "line", "lines", "poppy", "poppies",
			"poppyseed", "poppyseeds");

	private static final Unit BARLEYCORN = new Unit(8.46666e-3,
			UnitType.LENGTH, "Barleycorns", "barleycorn", "barleycorns",
			"barley", "barleys");

	// Hand
	private static final Unit DIGIT = new Unit(1.905e-2, UnitType.LENGTH,
			"Digits/Fingers", "digit", "digits", "finger", "fingers",
			"fingerbreadth", "fingerbreadths");

	private static final Unit HAND = new Unit(0.1016, UnitType.LENGTH, "Hands",
			"hand", "hands");

	private static final Unit NAIL = new Unit(5.715e-2, UnitType.LENGTH,
			"Nails", "nail", "nails", "Nail", "Nails");

	private static final Unit PALM = new Unit(7.62e-2, UnitType.LENGTH,
			"Palms/Handsbreadths", "palm", "palms", "handsbreadth",
			"handsbreadths");

	private static final Unit SHAFTMENT = new Unit(1.524e-3, UnitType.LENGTH,
			"Shaftments", "shaftment", "shaftments", "schaftmond",
			"scaeftemunde", "scaeftemunde");

	private static final Unit LINK = new Unit(.2012, UnitType.LENGTH,
			"Gunter's Links", "link", "links", "Link", "Links", "gunterslink",
			"gunterslinks");

	private static final Unit SPAN = new Unit(.2286, UnitType.LENGTH, "Spans",
			"span", "spans", "Span", "Spans");

	// French
	private static final Unit ARPENT = new Unit(58.471, UnitType.LENGTH,
			"Arpents", "arpent", "arpents");

	// MARINE
	private static final Unit FTM = new Unit(1.8288, UnitType.LENGTH,
			"Fathoms (ftm)", "fathom", "fathoms", "ftm");

	private static final Unit NMI = new Unit(1852, UnitType.LENGTH,
			"Nautical Miles (M, NM, nmi)", "M", "NM", "nmi", "nmile", "nmiles",
			"nauticalmile", "nauticalmiles", "seamile", "seamiles");

	private static final Unit CABLEUS = new Unit(219.456, UnitType.LENGTH,
			"Cable Lengths (US Navy)", "uscable", "uscables", "cableus",
			"cablesus", "uscablelength", "uscableslength");

	private static final Unit CABLEIN = new Unit(185.2, UnitType.LENGTH,
			"Cable Lengths (Int'l)", "intlcable", "cableintl",
			"intlcablelength", "intlcableslength");

	private static final Unit CABLEIM = new Unit(185.32, UnitType.LENGTH,
			"Cable Lengths (Imperial)", "cable", "cables", "cablelength",
			"cableslength", "impcable", "impcables", "impcablelength",
			"impcablelengths", "ukcable", "ukcables", "ukcablelength",
			"ukcableslength");

	// Surveying
	private static final Unit CHAIN = new Unit(20.1, UnitType.LENGTH, "Chains",
			"chain", "chains");

	private static final Unit ROD = new Unit(5, UnitType.LENGTH, "Rods", "rod",
			"rods", "pole", "poles", "perch", "perches");

	// Astronomy
	private static final Unit AU = new Unit(1495978707e2, UnitType.LENGTH,
			"Astronomical Units (AU)", "AU", "astunit", "astunits", "astrunit",
			"astrunits", "astronomicalunit", "astronomicalunits");

	private static final Unit ERADII = new Unit(6371e3, UnitType.LENGTH,
			"Earth Radii", "erad", "earthradius", "earthradii",
			"earthradiuses", "earthrad", "earthrads");

	private static final Unit LY = new Unit(94607304725808e2, UnitType.LENGTH,
			"Light Years (ly)", "ly", "lightyear", "lightyears");

	private static final Unit PARSEC = new Unit(308567758146719e2,
			UnitType.LENGTH, "Parsecs (pc)", "pc", "parsec", "parsecs");

	private static final Unit HUBBLE = new Unit(12263784135740165516689408D,
			UnitType.LENGTH, "Hubble Lengths (13.8bly)", "hubblelength",
			"hubblelengths");

	// Atomic
	private static final Unit A0 = new Unit(5.29e-10, UnitType.LENGTH,
			"Bohr Radii (a0)", "a0", "bohrradius", "bohrradii", "bohrradiuses",
			"atomiclength", "atomiclengths", "atomicunit", "atomicunits",
			"atomicunitoflength", "atomicunitsoflength");

	private static final Unit NATURAL = new Unit(5.29e-10, UnitType.LENGTH,
			"Natural Units of Length", "naturallength", "naturallengths",
			"naturalunit", "naturalunits", "naturalunitoflength",
			"naturalunitsoflength");

	private static final Unit PLENGTH = new Unit(1.61619926e-35,
			UnitType.LENGTH, "Planck Lengths", "plancklength", "planklengths");

	// Archaic
	private static final Unit CUBIT = new Unit(.4572, UnitType.LENGTH,
			"Cubits/Ells", "cubit", "cubits");

	private static final Unit ELL = new Unit(1.143, UnitType.LENGTH,
			"Ells (English)", "ell", "ells", "englishell", "englishells");

	private static final Unit FURLONG = new Unit(201, UnitType.LENGTH,
			"Furlongs", "furlong", "furlongs");

	private static final Unit PACE = new Unit(1.48, UnitType.LENGTH, "Paces",
			"pace", "paces");

	// Informal
	private static final Unit HORSE = new Unit(2.4, UnitType.LENGTH,
			"Horse Lengths", "horselength", "horses");

	private static final Unit GIRAFFE = new Unit(5, UnitType.LENGTH,
			"Giraffe Lengths (Avg)", "giraffe", "giraffes", "giraffelength",
			"giraffelengths");

	private static final Unit BEARDSECOND = new Unit(1e-8, UnitType.LENGTH,
			"Beard-Seconds", "beard-second", "beard-seconds", "beardsecond",
			"beardseconds");

	private final static ArrayList<Unit> LENGTH_UNITS = new ArrayList<Unit>(
			Arrays.asList(M, KM, CM, MM, MICROM, NM, FM, ARING, IN, THOU, FT,
					YD, MI, LEAGUE, LINE, BARLEYCORN, DIGIT, HAND, NAIL, PALM,
					SHAFTMENT, LINK, SPAN, ARPENT, FTM, NMI, CABLEUS, CABLEIN,
					CABLEIM, CHAIN, ROD, AU, ERADII, LY, PARSEC, HUBBLE, A0,
					NATURAL, PLENGTH, CUBIT, ELL, FURLONG, PACE, HORSE,
					GIRAFFE, BEARDSECOND));

	// MASS UNITS --------------------------------------------------------------
	// Common SI Units
	private static final Unit G = new Unit(1, UnitType.MASS, "Grams (g)", "g",
			"gram", "grams");

	private static final Unit KG = new Unit(1e3, UnitType.MASS,
			"Kilograms (kg)", "kg", "kilogram", "kilograms");

	private static final Unit MG = new Unit(1e-3, UnitType.MASS,
			"Milligrams (mg)", "mg", "milligram", "milligrams");

	private static final Unit MICROG = new Unit(1e-6, UnitType.MASS,
			"Micrograms (\u00B5g)", "\u00B5g", "microgram", "microograms");

	private static final Unit NG = new Unit(1e-9, UnitType.MASS,
			"Nanograms (ng)", "ng", "nanogram", "nanograms");

	private static final Unit PG = new Unit(1e-12, UnitType.MASS,
			"Picograms (pg)", "pg", "picogram", "pciograms");

	// Non-SI Metric
	private static final Unit TONNE = new Unit(1e6, UnitType.MASS,
			"Tonnes/Metric Tons (t)", "t", "tonne", "tonnes", "metricton",
			"metrictons", "metrictonne", "metrictonnes");

	// Avoirdupois
	private static final Unit LONGTON = new Unit(1016046.9088, UnitType.MASS,
			"Long Tons (2240lbs)", "longton", "longtons");

	private static final Unit TON = new Unit(907.18474, UnitType.MASS,
			"Short Tons (2000lbs, US Ton)", "ton", "tons", "uston", "ustons",
			"shortton", "shorttons");

	private static final Unit LONGHCWT = new Unit(50802.34544, UnitType.MASS,
			"Long Hundredweights (long cwt)", "longcwt", "longcwts",
			"longhundredweight", "longhundredweights");

	private static final Unit SHORTHCWT = new Unit(45359.237, UnitType.MASS,
			"Short Hundredweights (short cwt)", "shortcwt", "shortcwts",
			"shorthundredweight", "shorthundredweights");

	private static final Unit LONGQTR = new Unit(12700.58636, UnitType.MASS,
			"Long Quarters (long qtr)", "longqtr", "longqtrs", "longquarter",
			"longquarters");

	private static final Unit SHORTQTR = new Unit(11339.80925, UnitType.MASS,
			"Short Quarters (short qtr)", "shortqtr", "shortqtrs",
			"shortquarter", "shortquarters");

	private static final Unit STONE = new Unit(6350.29318, UnitType.MASS,
			"Stones (st)", "st", "stone", "stones");

	private static final Unit LB = new Unit(453.59237, UnitType.MASS,
			"Pounds (lb)", "lb", "lbs", "pound", "pounds");

	private static final Unit OZ = new Unit(28.349523, UnitType.MASS,
			"Ounces (oz)", "oz", "ounce", "ounces");

	private static final Unit DRAM = new Unit(1.771845195, UnitType.MASS,
			"Drachms/Drams (dr)", "dr", "drachm", "drachms", "dram", "drams");

	// Also a Troy weight.
	private static final Unit GRAIN = new Unit(0.06479891, UnitType.MASS,
			"Grains (gr)", "gr", "grain", "grains", "troygrain", "troygrains");

	// Troy Weights
	private static final Unit TROY = new Unit(373.2417216, UnitType.MASS,
			"Troy Pounds (troy)", "troy", "troys", "troypound", "troypounds",
			// Incorrect but acceptables:
			"troylb", "troylbs", "lbt", "lbst");

	private static final Unit OZT = new Unit(31.1034768, UnitType.MASS,
			"Troy Ounces (oz t)", "ozt", "ozst", "troyounce", "troyounces",
			"ouncetroy", "ouncestroy");

	private static final Unit DWT = new Unit(1.55517384, UnitType.MASS,
			"Pennyweights (dwt)", "dwt", "dwts", "pennyweight", "pennyweights");

	// Scientific
	private static final Unit AMU = new Unit(1.66053892173e-24, UnitType.MASS,
			"Unified Atomic Mass Units/Daltons (u/Da)", "u", "amu", "Da",
			"unifiedatomicmassunit", "unifiedatomicmassunits",
			"atomicmassunit", "atomicmassunits", "dalton", "daltons");

	private static final Unit EM = new Unit(9.1093821545e-28, UnitType.MASS,
			"Electron Rest Masses (me)", "me", "electronmass",
			"electronmasses", "electronrestmass", "electronrestmasses");

	private static final Unit PMASS = new Unit(2.1765113e-5, UnitType.MASS,
			"Planck Masses", "planckmass", "planckmasses");

	private static final Unit SOLAR = new Unit(1.98855e33, UnitType.MASS,
			"Solar Masses", "sun", "suns", "Sun", "Suns", "sunmass",
			"sunmasses", "massofsun", "solarmass", "solarmasses");

	private static final Unit EARTHMASS = new Unit(5.9736e27, UnitType.MASS,
			"Earth Masses", "earth", "earths", "earthmass", "earthmasses",
			"massofearth");

	// Other
	private static final Unit CARAT = new Unit(.2, UnitType.MASS,
			"Carats (CD)", "CD", "CDs", "carat", "carats");

	private final static ArrayList<Unit> MASS_UNITS = new ArrayList<Unit>(
			Arrays.asList(G, KG, MG, MICROG, NG, PG, TONNE, LONGTON, TON,
					LONGHCWT, SHORTHCWT, LONGQTR, SHORTQTR, STONE, LB, OZ,
					DRAM, GRAIN, TROY, OZT, DWT, AMU, EM, PMASS, SOLAR,
					EARTHMASS, CARAT));

	// MEMORY UNITS ------------------------------------------------------------
	private static final Unit BIT = new Unit(1, UnitType.MEMORY, "Bits", "bit",
			"bits");

	// SI Prefixes
	private static final Unit KILOBIT = new Unit(1e3, UnitType.MEMORY,
			"Kilobits (kbit)", "kbit", "kbits", "kilobit", "kilobits");

	private static final Unit MEGABIT = new Unit(1e6, UnitType.MEMORY,
			"Megabits (Mbit)", "Mbit", "Mbits", "megabit", "megabits");

	private static final Unit GIGABIT = new Unit(1e9, UnitType.MEMORY,
			"Gigabits (Gbit)", "Gbit", "Gbits", "gigabit", "gigabits");

	private static final Unit TERABIT = new Unit(1e12, UnitType.MEMORY,
			"Terrabits (Tbit)", "Tbit", "Tbits", "terabit", "terabits");

	private static final Unit PETABIT = new Unit(1e15, UnitType.MEMORY,
			"Petabits (Pbit)", "Pbit", "Pbits", "petabit", "petabits");

	private static final Unit EXABIT = new Unit(1e18, UnitType.MEMORY,
			"Exabits (Ebit)", "Ebit", "Ebits", "exabit", "exabits");

	private static final Unit ZETTABIT = new Unit(1e21, UnitType.MEMORY,
			"Zettabits (Zbit)", "Zbit", "Zbits", "zettabit", "zettabits");

	private static final Unit YOTTABIT = new Unit(1e24, UnitType.MEMORY,
			"Yottabits (Ybit)", "Ybit", "Ybits", "yottabit", "yottabits");

	// Binary Prefixes
	private static final Unit KIBIBIT = new Unit(Math.pow(2, 10),
			UnitType.MEMORY, "Kibibits (kibit)", "kib", "kibit", "kibits",
			"kibibit", "kibibits");

	private static final Unit MEBIBIT = new Unit(Math.pow(2, 20),
			UnitType.MEMORY, "Mebibits (Mibit)", "Mibit", "Mibits", "mebibit",
			"mebibits");

	private static final Unit GIBIBIT = new Unit(Math.pow(2, 30),
			UnitType.MEMORY, "Gibibits (Gibit)", "Gibit", "Gibits", "gibibit",
			"gibibits");

	private static final Unit TEBIBIT = new Unit(Math.pow(2, 40),
			UnitType.MEMORY, "Tebibits (Tibit)", "Tibit", "Tibits", "tebibit",
			"tebibits");

	private static final Unit PEBIBIT = new Unit(Math.pow(2, 50),
			UnitType.MEMORY, "Pebibits (Pibit)", "Pibit", "Pibits", "pebibit",
			"pebibits");

	private static final Unit EXBIBIT = new Unit(Math.pow(2, 60),
			UnitType.MEMORY, "Exbibits (Eibit)", "Eibit", "Eibits", "exbibit",
			"exbibits");

	private static final Unit ZEBIBIT = new Unit(Math.pow(2, 70),
			UnitType.MEMORY, "Zebibits (Zibit)", "Zibit", "Zibits", "zebibit",
			"zebibits");

	private static final Unit YOBIBIT = new Unit(Math.pow(2, 80),
			UnitType.MEMORY, "Yobibits (Yibit)", "Yibit", "Yibits", "yobibit",
			"yobibits");

	// Byte
	// SI Prefixes
	private static final Unit BYTE = new Unit(8, UnitType.MEMORY, "Bytes (B)",
			"B", "byte", "bytes", "octet", "octets");

	private static final Unit KILOBYTE = new Unit(8e3, UnitType.MEMORY,
			"Kilobytes (kB)", "kB", "kbyte", "kbytes", "kilobyte", "kilobytes");

	private static final Unit MEGABYTE = new Unit(8e6, UnitType.MEMORY,
			"Megabytes (MB)", "MB", "Mbyte", "Mbytes", "megabyte", "megabytes");

	private static final Unit GIGABYTE = new Unit(8e9, UnitType.MEMORY,
			"Gigabytes (GB)", "GB", "Gbyte", "Gbytes", "gigabyte", "gigabytes");

	private static final Unit TERABYTE = new Unit(8e12, UnitType.MEMORY,
			"Terabytes (TB)", "TB", "Tbyte", "Tbytes", "terabyte", "terabytes");

	private static final Unit PETABYTE = new Unit(8e15, UnitType.MEMORY,
			"Petabytes (PB)", "PB", "Pbyte", "Pbytes", "petabyte", "petabytes");

	private static final Unit EXABYTE = new Unit(8e18, UnitType.MEMORY,
			"Exabytes (EB)", "PB", "Ebyte", "Ebytes", "exabyte", "exabytes");

	private static final Unit ZETTABYTE = new Unit(8e21, UnitType.MEMORY,
			"Zettabytes (ZB)", "Zb", "Zbyte", "Zbytes", "zettabyte",
			"zettabytes");

	private static final Unit YOTTABYTE = new Unit(8e24, UnitType.MEMORY,
			"Yottabytes (YB)", "Yb", "Ybyte", "Ybytes", "yottabyte",
			"yottabytes");

	// Binary Prefixes
	private static final Unit KIBIBYTE = new Unit(Math.pow(2, 13),
			UnitType.MEMORY, "Kibibytes (kibyte)", "kibyte", "kibytes",
			"kibibyte", "kibibytes");

	private static final Unit MEBIBYTE = new Unit(Math.pow(2, 23),
			UnitType.MEMORY, "Mebibytes (Mibyte)", "Mibyte", "Mibytes",
			"mebibyte", "mebibytes");

	private static final Unit GIBIBYTE = new Unit(Math.pow(2, 33),
			UnitType.MEMORY, "Gibibytes (Gibyte)", "Gibyte", "Gibytes",
			"gibibyte", "gibibytes");

	private static final Unit TEBIBYTE = new Unit(Math.pow(2, 43),
			UnitType.MEMORY, "Tebibytes (Tibyte)", "Tibyte", "Tibytes",
			"tebibyte", "tebibytes");

	private static final Unit PEBIBYTE = new Unit(Math.pow(2, 53),
			UnitType.MEMORY, "Pebibytes (Pibyte)", "Pibyte", "Pibytes",
			"pebibyte", "pebibytes");

	private static final Unit EXBIBYTE = new Unit(Math.pow(2, 63),
			UnitType.MEMORY, "Exbibytes (Eibyte)", "Eibyte", "Eibytes",
			"exbibyte", "exbibytes");

	private static final Unit ZEBIBYTE = new Unit(Math.pow(2, 73),
			UnitType.MEMORY, "Zebibytes (Zibyte)", "Zibyte", "Zibytes",
			"zebibyte", "zebibytes");

	private static final Unit YOBIBYTE = new Unit(Math.pow(2, 83),
			UnitType.MEMORY, "Yobibytes (Yibyte)", "Yibyte", "Yibytes",
			"yobibyte", "yobibytes");

	private static final Unit NIBBLE = new Unit(4, UnitType.MEMORY, "Nibbles",
			"nibble", "nibbles", "nybble", "nybbles", "nyble", "nybles",
			"Nyble", "Nybles");

	private final static ArrayList<Unit> MEMORY_UNITS = new ArrayList<Unit>(
			Arrays.asList(BIT, KILOBIT, MEGABIT, GIGABIT, TERABIT, PETABIT,
					EXABIT, ZETTABIT, YOTTABIT, KIBIBIT, MEBIBIT, GIBIBIT,
					TEBIBIT, PEBIBIT, EXBIBIT, ZEBIBIT, YOBIBIT, BYTE,
					KILOBYTE, MEGABYTE, GIGABYTE, TERABYTE, PETABYTE, EXABYTE,
					ZETTABYTE, YOTTABYTE, KIBIBYTE, MEBIBYTE, GIBIBYTE,
					TEBIBYTE, PEBIBYTE, EXBIBYTE, ZEBIBYTE, YOBIBYTE, NIBBLE));

	// TIME UNITS --------------------------------------------------------------
	// SI Units
	private static final Unit S = new Unit(1, UnitType.TIME, "Seconds (s)",
			"s", "sec", "secs", "second", "seconds");

	private static final Unit MS = new Unit(1e-3, UnitType.TIME,
			"Milliseconds (ms)", "ms", "millisecond", "milliseconds");

	private static final Unit MICROS = new Unit(1e-6, UnitType.TIME,
			"Microseconds (\u00B5s)", "\u00B5s", "microsecond", "microoseconds");

	private static final Unit NS = new Unit(1e-9, UnitType.TIME,
			"Nanoseconds (ns)", "ns", "nanosecond", "nanoseconds");

	// Basic
	private static final Unit MIN = new Unit(60, UnitType.TIME,
			"Minutes (min)", "min", "mins", "minute", "minutes");

	private static final Unit HR = new Unit(36e2, UnitType.TIME, "Hours (hr)",
			"h", "hr", "hrs", "hour", "hours");

	private static final Unit DAY = new Unit(864e2, UnitType.TIME, "Days",
			"day", "days");

	private static final Unit WK = new Unit(6048e2, UnitType.TIME,
			"Weeks (wk)", "wk", "wks", "week", "weeks");

	private static final Unit MONTH = new Unit(2592e3, UnitType.TIME,
			"Months (30 days)", "month", "months");

	private static final Unit YR = new Unit(31536e3, UnitType.TIME,
			"Years (a/y/yr, 365 days)", "a", "y", "yr", "yrs", "year", "years");

	private static final Unit DECADE = new Unit(315576e3, UnitType.TIME,
			"Decades", "decade", "decades");

	private static final Unit CENTURY = new Unit(315576e4, UnitType.TIME,
			"Centuries", "century", "centuries");

	private static final Unit MILLENNIUM = new Unit(315576e5, UnitType.TIME,
			"Millennia", "millenium", "milleniums", "millenia");

	// Astronomical
	private static final Unit LUNARMONTH = new Unit(2350080, UnitType.TIME,
			"Lunar Months", "lunarmonth", "lunarmonths", "moon", "moons");

	private static final Unit GALACTIC = new Unit(7.25328e15, UnitType.TIME,
			"Galactic Years", "galacaticyear", "galacticyears", "galyear",
			"galyears");

	// Really Small
	private static final Unit JIFFY = new Unit(.01818181818, UnitType.TIME,
			"Jiffies", "jiffy", "jiffys", "jiffies");

	private static final Unit SHAKE = new Unit(1e-8, UnitType.TIME, "Shakes",
			"shake", "shakes");

	private static final Unit PTIME = new Unit(5.3910632e-44, UnitType.TIME,
			"Planck Time Units", "plancktimeunit", "planktimeunits");

	// Geological
	private static final Unit AGE = new Unit(31536e9, UnitType.TIME, "Ages",
			"age", "ages");

	private static final Unit EPOCH = new Unit(31536e10, UnitType.TIME,
			"Epochs", "epoch", "epochs");

	private static final Unit ERA = new Unit(31536e11, UnitType.TIME, "Eras",
			"era", "eras");

	private static final Unit EON = new Unit(1.5768e16, UnitType.TIME, "Eons",
			"eon", "eons");

	// Misc
	private static final Unit FORTNIGHT = new Unit(12096e2, UnitType.TIME,
			"Fortnights", "fortnight", "fortnight");

	private static final Unit KE = new Unit(864, UnitType.TIME, "Ke", "ke");

	private final static ArrayList<Unit> TIME_UNITS = new ArrayList<Unit>(
			Arrays.asList(S, MS, MICROS, NS, MIN, HR, DAY, WK, MONTH, YR,
					DECADE, CENTURY, MILLENNIUM, LUNARMONTH, GALACTIC, JIFFY,
					SHAKE, PTIME, AGE, EPOCH, ERA, EON, FORTNIGHT, KE));

	/**
	 * Returns the unit that corresponds with the name given. Returns null if
	 * unit is not found.
	 * 
	 * @param name
	 *            the name of the unit to be searched.
	 * @return Unit - Unit which corresponds to name given, or null if not
	 *         found.
	 */
	public static Unit getFromName(String name) {
		// Only acceleration contains "sq"
		if (name.contains("sq")) {
			for (Unit t : ACCELERATION_UNITS) {
				if (t.validNames.contains(name))
					return t;
			}
			return null;
		}

		// Only Acceleration and Current contain "per"
		if (name.contains("per")) {
			for (Unit t : ACCELERATION_UNITS) {
				if (t.validNames.contains(name))
					return t;
			}
			for (Unit t : CURRENT_UNITS) {
				if (t.validNames.contains(name))
					return t;
			}
			return null;
		}

		// Only Memory contains "byte" or "bit"
		if (name.contains("byte") || name.contains("bit")) {
			for (Unit t : MEMORY_UNITS) {
				if (t.validNames.contains(name))
					return t;
			}
			return null;
		}

		// Brute Force
		for (Unit t : LENGTH_UNITS) {
			if (t.validNames.contains(name))
				return t;
		}
		for (Unit t : MASS_UNITS) {
			if (t.validNames.contains(name))
				return t;
		}
		for (Unit t : TIME_UNITS) {
			if (t.validNames.contains(name))
				return t;
		}
		for (Unit t : ACCELERATION_UNITS) {
			if (t.validNames.contains(name))
				return t;
		}
		for (Unit t : CHARGE_UNITS) {
			if (t.validNames.contains(name))
				return t;
		}
		for (Unit t : CURRENT_UNITS) {
			if (t.validNames.contains(name))
				return t;
		}
		for (Unit t : MEMORY_UNITS) {
			if (t.validNames.contains(name))
				return t;
		}
		return null;
	}

	/**
	 * Searches the collection of Units of the given UnitType for the given
	 * name.
	 * 
	 * @param name
	 *            The name to search for.
	 * @param type
	 *            The collection of Units to search in.
	 * @return Unit - Unit which corresponds to the name given, or null if not
	 *         found.
	 */
	public static Unit getFromName(String name, UnitType type) {
		switch (type) {
		case ACCELERATION:
			for (Unit t : ACCELERATION_UNITS) {
				if (t.validNames.contains(name))
					return t;
			}
			break;
		case CHARGE:
			for (Unit t : CHARGE_UNITS) {
				if (t.validNames.contains(name))
					return t;
			}
			break;
		case CURRENT:
			for (Unit t : CURRENT_UNITS) {
				if (t.validNames.contains(name))
					return t;
			}
			break;
		case LENGTH:
			for (Unit t : LENGTH_UNITS) {
				if (t.validNames.contains(name))
					return t;
			}
			break;
		case MASS:
			for (Unit t : MASS_UNITS) {
				if (t.validNames.contains(name))
					return t;
			}
			break;
		case MEMORY:
			for (Unit t : MEMORY_UNITS) {
				if (t.validNames.contains(name))
					return t;
			}
			break;
		case TIME:
			for (Unit t : TIME_UNITS) {
				if (t.validNames.contains(name))
					return t;
			}
			break;
		}
		return null;
	}

	/**
	 * Returns the array which stores all Units of the given UnitType.
	 * 
	 * @param type
	 *            The UnitType to retrieve.
	 * @return ArrayList&lt;Unit&gt; - The collection of units of the given
	 *         UnitType, null if not found.
	 */
	public static ArrayList<Unit> getUnitSet(UnitType type) {
		switch (type) {
		case ACCELERATION:
			return ACCELERATION_UNITS;
		case CHARGE:
			return CHARGE_UNITS;
		case CURRENT:
			return CURRENT_UNITS;
		case LENGTH:
			return LENGTH_UNITS;
		case MASS:
			return MASS_UNITS;
		case MEMORY:
			return MEMORY_UNITS;
		case TIME:
			return TIME_UNITS;
		default:
			return null;
		}
	}
}