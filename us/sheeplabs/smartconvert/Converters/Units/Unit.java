package us.sheeplabs.smartconvert.Converters.Units;

import java.util.Arrays;
import java.util.HashSet;

/**
 * A Unit is a type of measurement from which and to which another can be
 * converted. Contains a HashSet of valid names, as HashSet can use the
 * contains() method efficiently for parsing. <br>
 * <br>
 * Unit types are Enum values which allow easy distinguishing of units and easy
 * collection of all units of a type. <br>
 * Units are hard-coded into the UnitLibrary class. <br>
 * <br>
 * 
 * @see UnitType
 * @see UnitLibrary
 * @author Michael Lambert
 * 
 */
public class Unit {
	protected final HashSet<String> validNames;
	protected final UnitType type;
	protected final String label;
	protected final double conversionFactor;

	/**
	 * Creates a new unit.
	 * 
	 * @param factor
	 *            Equivalent to the amount of the base unit in 1 of the unit
	 *            being made.
	 * @param type
	 *            An Enum which allows for easy distinguishing of units to which
	 *            this unit can be converted.
	 * @param label
	 *            the name of the unit.
	 * @param validNames
	 */
	protected Unit(double factor, UnitType type, String label,
			String... validNames) {
		this.conversionFactor = factor;
		this.type = type;
		this.label = label;
		this.validNames = new HashSet<String>(Arrays.asList(validNames));
	}

	/**
	 * Returns the label of the calling unit.
	 * 
	 * @return String - the unit name.
	 */
	public String getLabel() {
		return label;
	}

	/**
	 * Returns the conversion factor of the calling Unit.
	 * 
	 * @return double - the conversion factor of the Unit.
	 */
	public double getFactor() {
		return conversionFactor;
	}

	/**
	 * Returns the UnitType of the calling Unit.
	 * 
	 * @return UnitType - of the Unit.
	 */
	public UnitType getType() {
		return type;
	}

	/**
	 * Used for Documentation Generation.
	 * 
	 * @return HashSet&lt;String&gt; - Valid names for this unit.
	 */
	public HashSet<String> getNames() {
		return validNames;
	}
}