package us.sheeplabs.smartconvert.Converters.Units;

/**
 * Used for easy distinction between collections of Units in UnitLibrary.
 * Current Types:
 * <ul>
 * <li>ACCELERATION ("Acceleration")</li>
 * <li>CHARGE ("Charge")</li>
 * <li>CURRENT ("Current")</li>
 * <li>LENGTH("Length")</li>
 * <li>MASS("Mass")</li>
 * <li>MEMORY("Memory")</li>
 * <li>TIME("Time")</li>
 * </ul>
 * 
 * @author Michael Lambert
 */
public enum UnitType {
	ACCELERATION("Acceleration"), CHARGE("Charge"), CURRENT("Current"),
		LENGTH("Length"), MASS("Mass"), MEMORY("Memory"), TIME("Time");

	private String name;

	private UnitType(String name) {
		this.name = name;
	}

	/**
	 * Returns the name of the UnitType.
	 * 
	 * @return String the name of the UnitType.
	 */
	public String getName() {
		return name;
	}
}
