package us.sheeplabs.smartconvert.Converters;

import java.math.BigDecimal;
import java.math.MathContext;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Locale;

import us.sheeplabs.smartconvert.Converters.Units.Unit;
import us.sheeplabs.smartconvert.Converters.Units.UnitLibrary;
import us.sheeplabs.smartconvert.Parser.InvalidInputException;

/**
 * Solves a conversion problem. Will give an array of results based on the unit
 * given, and an array of errors if requested.
 * 
 * @author Michael Lambert
 */
public class Converter {
	// Base Unit
	private double number;
	private Unit unitFrom;

	// Units to convert to.
	private ArrayList<Unit> unitsTo;
	private String[] resultLabels = new String[50];
	private String[] results;

	// Errors
	private int errorCount = 0;
	private String[] errors = new String[50];

	/**
	 * Converts to every unit that is the same type as the unit given.
	 * 
	 * @param num
	 *            The number of base units from which to convert
	 * @param unit
	 *            The base unit from which to convert.
	 */
	public Converter(double num, Unit unit) {
		this.number = num;

		// Catches numbers that exceed double precision.
		if (Double.isInfinite(number))
			throw new InvalidInputException("Number too large/small.");

		this.unitFrom = unit;

		// Creates list of units to convert to.
		this.unitsTo = new ArrayList<Unit>();
		int i = 0;
		for (Unit nextUnit : UnitLibrary.getUnitSet(unit.getType())) {
			if (nextUnit != unitFrom) {
				resultLabels[i++] = nextUnit.getLabel();
				unitsTo.add(nextUnit);
			}
		}
	}

	/**
	 * Initializes a converter which converts to the units specified. Keeping
	 * the units to convert to as a String array up to this point allows easy
	 * filtering of invalid units as errors.
	 * 
	 * @param num
	 *            The number of base units from which to convert
	 * @param unit
	 *            The base unit from which to convert.
	 * @param unitStrings
	 *            An array of unparsed units to convert to.
	 */
	public Converter(double num, Unit unit, String[] unitStrings) {
		this.number = num;

		// Catches numbers that exceed double precision.
		if (Double.isInfinite(number))
			throw new InvalidInputException("Number too large/small.");

		// Parses units and creates a list of valid/invalid units.
		this.unitFrom = unit;
		this.unitsTo = new ArrayList<Unit>();
		Unit nextUnit;
		for (int i = 0; i < unitStrings.length; i++) {
			nextUnit = UnitLibrary.getFromName(unitStrings[i], unit.getType());
			if (nextUnit == null) {
				errors[errorCount++] = "\""
						+ unitStrings[i]
						+ "\" is not a valid unit of "
						+ unitFrom.getType().getName()
								.toLowerCase(Locale.getDefault()) + ".";
			} else {
				if (!unitsTo.contains(nextUnit) && nextUnit != unitFrom) {
					unitsTo.add(nextUnit);
					resultLabels[i - errorCount] = nextUnit.getLabel();
				}
			}
		}
	}

	/**
	 * Converts and returns results in the form of a String array.
	 * 
	 * @return String[] - the converted units.
	 */
	public String[] solve() {
		// Determines the precision used in the conversion.
		DecimalFormat normFormat = new DecimalFormat(
				"#############################################################"
						+ ".#############################################################");
		// The format of numbers converted to scientific notation.
		DecimalFormat sciFormat = new DecimalFormat("#########0.0#########E0");

		results = new String[unitsTo.size()];
		int count = 0;

		for (Unit nextUnit : unitsTo) {
			// Calculates result quickly.
			double result = number
					* (unitFrom.getFactor() / nextUnit.getFactor());

			// Hides loss of precision errors.
			if (!Double.isInfinite(result) && !Double.isNaN(result)) {
				BigDecimal rounding = new BigDecimal(result,
						MathContext.DECIMAL128);

				// Used to check length for format.
				String droppedZeroes = normFormat
						.format(rounding.doubleValue());

				// Scientific Notation if there are more than 10 digits.
				if (droppedZeroes.length() > 10) {
					String formattedString = sciFormat.format(rounding
							.doubleValue());
					if (sciFormat.format(rounding.doubleValue()).contains("E0"))
						results[count] = sciFormat.format(
								rounding.doubleValue()).substring(0,
								formattedString.length() - 2);
					else
						results[count] = formattedString;
				} else
					results[count] = droppedZeroes;
			} else
				results[count] = "Number too large/small";
			++count;
		}
		return results;
	}

	/**
	 * Returns the labels for the results screen. Labels should be set in the
	 * constructor.
	 * 
	 * @return String[] - Unit labels to be converted to.
	 */
	public String[] getLabels() {
		return resultLabels;
	};

	/**
	 * Returns the number of errors encountered while solving.
	 * 
	 * @return int - number of errors
	 */
	public int getErrorCount() {
		return errorCount;
	}

	/**
	 * Returns the error strings created during "solve"
	 * 
	 * @return String[] - the error messages.
	 */
	public String[] getErrors() {
		return errors;
	}

	/**
	 * Returns the parser interpretation of user input.
	 * 
	 * @return String - Parser interpretation of input.
	 */
	public String getInterpretation() {
		return number + " " + unitFrom.getLabel();
	}
}