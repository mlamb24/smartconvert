package us.sheeplabs.smartconvert;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.support.v4.app.NavUtils;
import android.annotation.TargetApi;
import android.graphics.Typeface;
import android.os.Build;

public class Help extends Activity {

	private TextView input_label, input_text, sciNot_label, sciNot_text, unit_text;
	private Typeface typeFace;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_help);
		
		input_label = (TextView)findViewById(R.id.inputlabel);
		input_text = (TextView)findViewById(R.id.inputtext);
		sciNot_label = (TextView)findViewById(R.id.sciNot_label);
		sciNot_text = (TextView)findViewById(R.id.sciNot_text);
		unit_text = (TextView)findViewById(R.id.unit_text);
		
		typeFace = Typeface.createFromAsset(getAssets(), "fonts/RobGraves.ttf");
		
		input_label.setTypeface(typeFace);
		input_text.setTypeface(typeFace);
		sciNot_label.setTypeface(typeFace);
		sciNot_text.setTypeface(typeFace);
		unit_text.setTypeface(typeFace);
	}

	/**
	 * Set up the {@link android.app.ActionBar}, if the API is available.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	private void setupActionBar() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			getActionBar().setDisplayHomeAsUpEnabled(true);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.help, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

}
