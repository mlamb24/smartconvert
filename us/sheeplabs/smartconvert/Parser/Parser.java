package us.sheeplabs.smartconvert.Parser;

import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import us.sheeplabs.smartconvert.Converters.Converter;
import us.sheeplabs.smartconvert.Converters.Units.Unit;
import us.sheeplabs.smartconvert.Converters.Units.UnitLibrary;

/**
 * Parses general input to make a Converter object.
 * 
 * @author Michael Lambert
 */
public class Parser {

	/**
	 * Parses the input and returns a Converter based upon this input.
	 * 
	 * @param input
	 *            the input which to parse
	 * @return Converter - Corresponds with the given input.
	 * @throws InvalidInputException
	 *             if the number is not a proper double within the valid range
	 *             or if the unit to convert from is invalid.
	 */
	public static Converter parseInput(String input) {
		Pattern p = Pattern.compile(
		// Number
				"(\\-*[\\d]+(?:[.]\\d+)?(?:[eE]\\-?\\d*)?)\\s*" +
				// unitFrom with a negative lookahead for to or in
						"((?:[a-zA-Z��ŵ./^23\\- ](?![tT][oO]|[iI][nN]))+)" +
						// "to" or "in" + unitsTo
						"(?:\\s+(?:[tT][oO]|[iI][nN])\\s+([a-zA-Z��ŵ./^23,\\s]+))?");

		Matcher m = p.matcher(input);
		if (m.find()) {

			// Parsing of the amount of the base unit to convert from.
			double number;
			try {
				if (m.group(1).split("[eE]").length == 2) {
					// Catches numbers to small to convert to.
					if (Integer.parseInt(m.group(1).split("[eE]")[1]) < -100)
						throw new InvalidInputException(
								"Please use an exponent greater than -100 for results other than 0.");
				}
				number = Double.parseDouble(m.group(1));
			} catch (NumberFormatException nfe) {
				throw new InvalidInputException(m.group(1)
						+ " is not a valid number.");
			}

			// Formats and stores the unit to convert from.
			String unitString = formatString(m.group(2));
			Unit unitFrom = UnitLibrary.getFromName(unitString);
			if (unitFrom == null)
				throw new InvalidInputException(m.group(2)
						+ " is not a valid unit.");

			// Formats and stores the units to convert to, if necessary.
			if (!(m.group(3) == null || m.group(3).isEmpty())) {
				String[] unitsTo = m.group(3).split("[,;]");
				for (int i = 0; i < unitsTo.length; i++) {
					unitsTo[i] = formatString(unitsTo[i]);
				}
				return new Converter(number, unitFrom, unitsTo);
			}

			// If unitsTo are not found, a vague converter is returned.
			return new Converter(number, unitFrom);
		} else
			throw new InvalidInputException(
					"The system cannot read your input.  Please try again.");
	}

	/**
	 * Formats the unit String in such a way that it is easy to parse. <br>
	 * <br>
	 * Formatting Used:
	 * <ul>
	 * <li>Removes all whitespace from the given input.</li>
	 * <li>Makes all input of 6 or more characters lowercase.</li>
	 * <li>"metre" is automatically converted in parser to "metre"</li>
	 * <li>"squared" and "square" become "sq"</li>
	 * <li>"cubed", "cube" and "cubic" become "cu"</li>
	 * </ul>
	 * 
	 * @return String - A formatted input String.
	 */
	private static String formatString(String input) {
		String formattedInput = input.replaceAll("[.\\s^*']", "");

		// If the string is 6 characters or more, it is not a
		// case-sensitive abbreviation.
		if (formattedInput.length() >= 6)
			formattedInput = new String(formattedInput
					.toLowerCase(Locale.getDefault()).replaceAll("\\s+", "")
					.replaceAll("\\/", "per")
					.replaceAll("metre", "meter")
					.replaceAll("squared|square", "sq")
					.replaceAll("cubed|cube|cubic", "cb"));
		else
			formattedInput = new String(formattedInput
					.replaceAll("\\/", "per")
					.replaceAll("metre", "meter")
					.replaceAll("square", "sq")
					.replaceAll("cubed|cube|cubic", "cb"));
		return formattedInput;
	}
}
