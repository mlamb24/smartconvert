package us.sheeplabs.smartconvert.Parser;

@SuppressWarnings("serial")
/**
 * This exception is thrown when invalid input is given. 
 * @author Michael Lambert
 */
public class InvalidInputException extends RuntimeException {

    /**
     * The basic constructor.
     *
     * @see RuntimeException
     */
    public InvalidInputException() {
    }

    /**
     * Method inherited from RuntimeException.
     *
     * @param detailMessage the message to be given when thrown.
     * @see RuntimeException
     */
    public InvalidInputException(String detailMessage) {
        super(detailMessage);
    }
}
